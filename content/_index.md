# Olá, eu sou Gabriel

Sou músico e meu instrumento principal é o violão de 6 cordas, mas também toco guitarra, tenho um pouco de experiência em teclado e sei tocar flauta doce. Em geral, me dou bem com instrumentos de cordas. Minha jornada musical começou na internet, onde aprendi os fundamentos do instrumento e tocava músicas mais simples. No entanto, queria aprofundar meu conhecimento e entender a teoria musical, estudar música de fato. Por isso, me matriculei na **Escola de Música Vila Lobos**, uma instituição renomada no Rio de Janeiro, reconhecida por sua excelência no ensino musical e por formar diversos músicos talentosos.

{{< figure src="images/ilustativo.jpg" width="100%" >}}

## Experiência Musical

Na escola, tive a oportunidade de trabalhar com o incrível professor João Zainko que me convidou para participar da **Orquestra de Violões Harmonium**, uma experiência enriquecedora que ampliou minha compreensão musical e me permitiu colaborar com outros músicos. 

Durante meu tempo na orquestra, escrevi arranjos de músicas tanto para violão solo quanto para conjuntos de quatro violões. Essa experiência não apenas desenvolveu minhas habilidades de arranjo, mas também me proporcionou uma plataforma para explorar diferentes estilos musicais e trabalhar em equipe.

{{< youtube vBjsLLwbRnY >}}

## Projetos e Arranjos

### Arranjos para Violão Solo
Exemplos de arranjos que fiz:
- **Carinhoso** (Pixinguinha)
- **Canto de Ossanha** (Vinícius de Moraes/Baden Powell)
- **Deixa** (Baden Powell)
- **Berimbau/Consolação** (Vinícius de Moraes/Baden Powell)

### Arranjos para Quatro Vozes
- **Carinhoso** (Pixinguinha)
- **João e Maria** (Chico Buarque)

Alguns desses arranjos podem ser acessados através do menu de navegação.

## Habilidades

- Composição e arranjo musical
- Performance ao vivo
- Colaboração em grupos musicais


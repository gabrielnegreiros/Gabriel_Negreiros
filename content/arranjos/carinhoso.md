---
title: 'Carinhoso'
date: 2024-10-12T14:40:49-03:00
draft:  false
arr: true
voz: "violão solo."
compositor: "Pixinguinha"
---
"Carinhoso" é uma das composições mais icônicas da música brasileira, escrita por **Pixinguinha** em 1917, com letra de **Braguinha**. A canção é um exemplo clássico do estilo de chorinho, refletindo a melodia suave e nostálgica que se tornou um símbolo de amor e afeto no Brasil.

Sendo tão marcante, decidi que teria que escrever um arranjo para canção. Busquei inspirações em gravações antigas e outros arranjos na internet. Uma curiosidade: está estampado nas paredes da estação de metrô Nossa Senhora da Paz (Rio de Janeiro) a partitura dessa música. Usei isso para escrever parte do meu arrranjo.

{{< youtube KMXJWrwRR1c >}}
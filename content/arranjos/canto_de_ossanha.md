---
title: "Canto de Ossanha"
date: 2021-09-18T23:28:40-03:00
draft: false
arr: true
voz: "violão solo."
compositor: "Vinícius de Moraes/Baden Powell"
---
"Canto de Ossanha" é uma famosa composição de **Vinicius de Moraes** e **Baden Powell**, que mistura elementos da música afro-brasileira com a poesia característica do autor. A canção fala sobre as tradições e o espírito do orixá Ossanha, refletindo a rica cultura e a diversidade musical do Brasil.

Escolhi escrever um arranjo para "Canto de Ossanha" após um professor me convidar para uma apresentação. Sempre gostei muito da música, então aproveitei a oportunidade para arranjar. 

{{< youtube QU4pY4IPjmw >}}
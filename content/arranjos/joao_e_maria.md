---
title: "João e Maria"
date: 2021-09-18T23:27:57-03:00
draft: false
arr: true
voz: "quatro vozes."
compositor: "Sivuca/Chico Buarque"
---
"João e Maria" é uma música composta por **Sivuca** em 1947. A letra foi escrita por **Chico Buarque** em 1977 para o musical "Os Saltimbancos". Pela música ter sido composta próxima de seu nascimento, Chico escreveu sua letra como um tema infantil.

Decidi escrever um arranjo para "João e Maria" por ser minha música favorita. Escrever esse arranjo para quatro vozes foi o ideal para conseguir tranmitir todo sentimento e beleza dessa canção.

{{< youtube l9vLkoQiBgA >}}


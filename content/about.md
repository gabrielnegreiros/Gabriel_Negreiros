+++
title = 'Minha história com a música'
date = 2024-10-05T16:32:35-03:00
draft = false
arr = false
+++

A música sempre esteve presente na minha vida. Meu avô era o músico da família e sempre reunia todos para tocar e cantar. Cresci apreciando a música brasileira. Minha mãe adorava cozinhar ouvindo Chico Buarque, Cazuza, Nando Reis, Rita Lee e tantos outros artistas incríveis, que moldou meu gosto musical.

Minha jornada aprendendo música começou na infância, quando aprendi a tocar flauta doce no colégio. Depois, experimentei o teclado, mas foi somente quando comecei a aprender violão que realmente me apaixonei por tocar música. Desde então, venho explorando diversos estilos e aperfeiçoando minhas habilidades e conhecimentos.

Além do violão, também toco guitarra e me dou bem com instrumentos de cordas em geral. Essa versatilidade me permite experimentar diferentes sonoridades e enriquecer minhas composições.

## Artistas que aprecio

Os artistas que escuto e gosto muito incluem:
- **Chico Buarque**
- **Rita Lee**
- **Charlie Brown Jr.**
- **Cazuza**
- **Nando Reis**
- **Barão Vermelho**
- **Legião Urbana**
- **Seu Jorge**
- **Gonzaguinha**
- **Djavan**
- **Paulinho Nogueira**
- **Baden Powell**
- **Tim Maia**

Esses músicos refletem minha paixão pela diversidade da música brasileira e suas influências na minha própria criação.

## Objetivos musicais

Tenho o desejo de aprender outros instrumentos e estudar regência. Meu próximo instrumento a aprender será o acordeon ou o violino. Estou sempre em busca de novos desafios, seja compondo, arranjando ou colaborando com outros músicos. Acredito que a música é uma linguagem universal e quero compartilhar essa paixão com o mundo.